from setuptools import setup

setup(
    name="ga_mca_git_hooks",
    version="0.1.3",
    install_requires=[
        "black==22.8.0",
    ],
    scripts=[
        "scripts/ga-mca-pre-commit",
        "scripts/ga-mca-reformat-helper",
    ],
)
