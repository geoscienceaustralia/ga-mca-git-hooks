# GA MCA Git Hooks

This repository contains scripts used as git pre-commit hooks in various source
repositories managed by the GA MCA Support Team.

It is distributed as a Python package to make it easy to install (since we are
all already doing python development anyway).

## Functionality

Currently, the hooks provide a single function: before committing changes, it
reformats code to a consistent style. This is done for the following languages:

- For Python, [black](https://black.readthedocs.io/en/stable/) is used to
  produce PEP-8 compliant code. See
  [here](https://black.readthedocs.io/en/stable/the_black_code_style/current_style.html)
  for more details on the black code style.
- For Terraform, the built-in `terraform fmt` is used.

When you invoke `git commit`, any files of these types with staged changes will
be reformatted in both the working tree and the index.

## Dependencies

To use the python reformatter, all you should need is a Python 3 installation
with a working pip - installing this package will automatically install black
as a dependency. To use the Terraform reformatter, you'll need Terraform
installed separately.

## Usage

### Setting up hooks in a repository for the first time

If you're starting a new project from scratch, you can skip to step 3.

1. If possible, ensure any pending branches/PRs have been merged, to avoid
   having to review diffs that include formatting changes.

2. Reformat the existing code in the repository:

   ```sh
   terraform fmt -recursive .
   black .
   ```

   Commit this change to the repository.

3. Copy the script [`./install-hooks.sh`](./install-hooks.sh) into the root of
   the repository, add a note to the README instructing developers to run the
   script after cloning, add the line `/.reformat_backup` to the repository's
   `.gitignore` file, and commit.

### Enabling hooks in your working copy

After cloning the repository, each team member will have to manually install
the hooks in their working copy, which should be as simple as running
`./install-hooks.sh`.

(Making this happen automatically is impossible, since allowing this would mean
git clone could execute arbitrary code!)
